---
title: "「Notion」个人年度订阅服务总览模板共享"
categories: [ "程序人生" ]
tags: [ 'Notion' ]
draft: false
slug: "notion-subs"
date: "2021-02-24 10:16:00"
---

今天逛[罗磊博客](https://luolei.org)，发现了一个有意思的订阅服务管理notion模版，就直接复制过来了，在此感谢罗磊老师提供的模版

可以参考「我的年度订阅」，一年算下来大概9000不到的样子，算是不少了，主要开支都在软件服务订阅上，个人在云上花费的钱比较少。

[前往 Notion 链接](https://taurusxin.notion.site/ae97700d125d4465abe9725d16d7b858?v=f592578996724e7087895c58a7500030&pvs=4)

![订阅服务总览](https://cdn.taurusxin.com/old-cdn/typecho/2021/02/24/Xnip2021-02-24_10-19-40.png)
