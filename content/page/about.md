---
title: 关于
description: 关于我的一些信息
date: '2021-12-10'
aliases:
  - about
  - contact
slug: about
lastmod: '2025-02-04T11:08:00+0800'
menu:
    main: 
        weight: 2
        params:
            icon: user
---

## 关于我

我是 TaurusXin，来自杭州，计算机/通信工程硕士

爱好咖啡、旅行、摄影

宽带症候群（对于网络要求极高的人群），Apple 全家桶用户

## 职业方向

- Web 全栈开发
- 移动应用开发
- Linux 运维
- 深度学习 / 计算机视觉
- 网络工程

## 职业

|  开始        | 结束 | 职位    |
|------------ |---- |-------|
| 2024 年 2 月 | 至今 | 运维工程师 |

## 代码托管

- [GitHub - taurusxin](https://github.com/taurusxin)
- [GitLab - taurusxin](https://gitlab.com/taurusxin)
- [自建 Gitea](https://git.taurusxin.com)

## 联系方式

### 邮箱

- [域名邮箱: i@taurusxin.com](mailto:i@taurusxin.com)
- [Outlook: taurusxin@outlook.com](mailto:taurusxin@outlook.com)

### IM

- [Telegram](https://t.me/taurusxin)

## 其他

### 自媒体

- [Bilibili](https://space.bilibili.com/4360325)

### 游戏

- Minecraft
  - Java Edition - [Taurus_Xin](https://namemc.com/profile/Taurus_Xin.1)
  - Bedrock Edition - TaurusXin6015
- [Steam](https://steamcommunity.com/id/taurusyx/)
