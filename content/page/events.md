---
title: 事件
description: 关于此博客的一些包含但不限于的技术性事件
date: '2022-05-31'
aliases:
  - events
slug: events
lastmod: '2024-08-20T00:32:00+0800'
menu:
    main: 
        weight: 8
        params:
            icon: notebook
---

## 大事纪

### 2024 年

- 2024 年 8 月 20 日 - 将权威 DNS 从「华为云 DNS」切换至「腾讯云 DNS 专业版」，拥有更多的全球集群节点，更快的解析响应速度。
- 2024 年 7 月 21 日 - 由于之前的 bootcdn 供应链投毒事件，所有的静态资源现已自托管至 cdn.taurusxin.com，网站首屏时间明显变快。
- 2024 年 5 月 3 日 - 停止 gravatar.taurusxin.com 全球头像镜像服务

### 2023 年

- 2023 年 12 月 11 日 - 修正图片相册功能，CDN 后的图片也能生成图册并且互动，详见[这个提交](https://gitlab.com/taurusxin/hugo/-/commit/48a7c9a3299dcae5e59345fa7d2e2961cb5a63ab)
- 2023 年 7 月 23 日 - 新增留言板页面，详见[这篇文章](https://www.taurusxin.com/new-message-board/)
- 2023 年 7 月 17 日 - 升级 Gravatar 镜像站 CDN，采用 CacheFly 全球 CDN，详见[这篇文章](https://www.taurusxin.com/gravatar/)
- 2023 年 3 月 5 日 - 启用了全新的评论系统 Artalk，详见[这篇文章](https://www.taurusxin.com/artalk-comment/)
- 2023 年 3 月 1 日 - 域名前加上 www，优化 SEO，并 301 原先的根域名

### 2022 年

- 2022 年 9 月 1 日 - 接入备案，并分流接入全球 CDN (境外 Cloudflare + 大陆腾讯云 CDN)，详见[这篇文章](https://www.taurusxin.com/global-cdn-beian/)
- 2022 年 6 月 13 日 - 启用 taurusxin.com 新域名
- 2022 年 5 月 31 日 - 新增此页面，记录过去博客发生的所有变化

### 2021 年

- 2021 年 12 月 21 日 - 抛弃 Typecho，迁移所有内容至 [Hugo](https://gohugo.io/) 静态博客，使用 [Stack](https://github.com/CaiJimmy/hugo-theme-stack) 作为主题，托管至 Cloudflare Pages，同时域名去掉了 blog 开头，使用根域名
- 2021 年 8 月 26 日  - 对 Cloudflare 做 Anycast IP 优选，确保大陆接入速度，全站接入 IPv6
- 2021 年 3 月 14 日 - 开启 Cloudflare 的全站 HTTP3 QUIC 支持
- 2021 年 3 月 13 日 - 博客接入 Cloudflare 全球 CDN

### 2020 年

- 2020 年 10 月 7 日 - 博客主题变更为 [handsome](https://www.ihewro.com/archives/489/) 正版付费主题
- 2020 年 8 月 20 日 - 因升级 Linux 依赖及核心版本，服务器无法正常启动，VNC 连接后解决，详见 <https://www.taurusxin.com/2020-server-down/>

### 2018 年

- 2018 年 8 月 5 日 - 购入阿里云香港轻量应用服务器，使用 [Typecho](https://typecho.org/) 作为博客程序，[Mirages](https://get233.com/archives/mirages-intro.html) 作为主题
